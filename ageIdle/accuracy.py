# %%
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

plt.style.use('../cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

intensityScale = 2

# %%
ad = np.load(f'{cdir}/assigndata{intensityScale}.npy')
print(ad.shape)
im = plt.imshow(ad)
plt.colorbar(im)
plt.xlabel('Neuron')
plt.ylabel('Example')

# %%
input_numbers = y_train[:ad.shape[0]]

assignments = np.ones(100) * -1

maximum_rate = [0] * 100

for j in range(10):
    rate = np.sum(ad[input_numbers == j],
                  axis=0) / (input_numbers == j).sum()
    for i in range(100):
        if rate[i] > maximum_rate[i]:
            maximum_rate[i] = rate[i]
            assignments[i] = j
# %%
plt.scatter(range(100), assignments, color='black')
plt.ylabel('Assignment')
plt.xlabel('Neuron')

# %%
plt.hist(assignments)

# %%
plt.bar(range(100), maximum_rate, color='black')
plt.ylabel('Maximum rate')

# %%


def getRecognizedNumber(spike_rates):
    summed_rates = [(np.sum(spike_rates[assignments == i]) / (assignments == i).sum(
    ) if (assignments == i).sum() > 0 else -1) for i in range(10)]
    res = np.argsort(summed_rates)[-1]
    return res if summed_rates[res] > 0 else -1


# %%
ts = np.logspace(3, 5, num=9)

accuracy = []

for t in ts:
    try:
        ed = np.load(f'{cdir}/eval/evaldata{intensityScale}_{t}.npy')
        rn = np.array([getRecognizedNumber(ed[i, :])
                      for i in range(ed.shape[0])])
        print(f'{t} {(rn == y_test[:ed.shape[0]]).mean()}')
        accuracy.append((rn == y_test[:ed.shape[0]]).mean())

        im = plt.imshow(ed)
        plt.colorbar(im)
        plt.title('evaldata' + str(t))
        plt.xlabel('Neuron')
        plt.ylabel('Image')
        plt.show()
    except:
        accuracy.append(np.NaN)


# %%
ed = np.load(f'{cdir}/eval/evaldata{intensityScale}_0.0.npy')

# %%
im = plt.imshow(ed)
plt.colorbar(im)
plt.title('evaldata0.0')
plt.xlabel('Neuron')
plt.ylabel('Image')

# %%
rn = np.array([getRecognizedNumber(ed[i, :])
               for i in range(ed.shape[0])])
acc0 = (rn == y_test[:ed.shape[0]]).mean()
print(f'0.0 {acc0}')

# %%
plt.scatter(y_test[:ed.shape[0]], rn)
plt.xlabel('True number')
plt.ylabel('Recognized number')

# %%
rn.shape

# %%

acc = {
    'accuracy': accuracy,
    'ts': ts,
    'acc0': acc0
}
pickle.dump(acc, open(f'{cdir}/accuracy.pickle', 'wb'))

# %%
plt.scatter(ts, accuracy, color='black')

# %%
