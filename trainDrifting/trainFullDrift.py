import os
from uuid import uuid4

from keras.datasets import mnist
from Model import *

(X_train, y_train), (X_test, y_test) = mnist.load_data()

train_items = 40000

runid = 'run_drifting_' + uuid4().hex
os.mkdir(f'weights/{runid}')
os.mkdir(f'states/{runid}')

seed(0)

print('Starting')

model = Model()
print('Model created')
model.net.restore(filename=f'trainfull0.b2')
model.driftEnabled = True

print('Model loaded')

for i in range(train_items):
    model.train(X_train[i])
    if ((i+1) % 1000 == 0):
        model.net.store(filename=f'states/{runid}/trainfull{i+1}.b2')
        np.save(f'weights/{runid}/trainfull{i+1}.npy', model.getWeights())
    print(f'Done with {i+1} of {train_items}')
