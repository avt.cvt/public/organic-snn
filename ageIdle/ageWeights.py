# %%
import os

import numpy as np
from scipy.integrate import odeint

cdir = os.path.dirname(os.path.realpath(__file__))

# %%


def ageWeights(t):
    weights = np.load(f'{cdir}/../inputs/weightsAfterDriftingTraining.npy')

    def age(w0):
        def dwdt(w, _):
            return -4e-7*(np.exp(8*w) - np.exp(-8*w))

        return odeint(dwdt, w0, [0, t])[1]

    for (i, j), w in np.ndenumerate(weights):
        weights[i, j] = age(w)
        print(i, j, weights[i, j])

    np.save(f'{cdir}/weights/weightsAge{t}.npy', weights)


# %%
ageWeights(0.0)

# %%
for t in np.logspace(3, 5, num=9):
    ageWeights(t)

# %%
# for i in range(1, 11):
#     t = i*1000
#     ageWeights(t)
