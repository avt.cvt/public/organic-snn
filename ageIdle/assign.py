# %%
import os
import sys

import matplotlib.pyplot as plt
from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

sys.path.insert(0, '..')
if True:
    from Model import *

cdir = os.path.dirname(os.path.realpath(__file__))

# %%
model = Model()
model.net.restore(filename=f'{cdir}/../inputs/stateAfterDriftingTraining.b2')

# %%
features = []
intensityScale = 2
for i in range(1000):
    print(f'Evaluating {i}')
    f = model.evaluate(X_train[i], intensity_scale=intensityScale)
    features.append(f)


features = np.array(features)
np.save(f'{cdir}/assigndata{intensityScale}.npy', features)
