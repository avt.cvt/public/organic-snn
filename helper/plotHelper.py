import matplotlib.pyplot as plt
import numpy as np


def set_size(w=88, h=88, ax=None):
    """ w, h: width, height in inches """
    w *= 0.0393701
    h *= 0.0393701
    if not ax:
        ax = plt.gca()
    l = ax.figure.subplotpars.left
    r = ax.figure.subplotpars.right
    t = ax.figure.subplotpars.top
    b = ax.figure.subplotpars.bottom
    figw = float(w)/(r-l)
    figh = float(h)/(t-b)
    ax.figure.set_size_inches(figw, figh)


def weight_2d_arrangement(weights):
    n_e_sqrt = int(np.sqrt(100))
    n_in_sqrt = int(np.sqrt(28**2))
    num_values_col = n_e_sqrt*(n_in_sqrt)
    num_values_row = num_values_col
    rearranged_weights = np.nan * np.ones((num_values_col, num_values_row))

    for i in range(n_e_sqrt):
        for j in range(n_e_sqrt):
            rearranged_weights[i*n_in_sqrt: (i+1)*n_in_sqrt, j*n_in_sqrt: (j+1)*n_in_sqrt] = \
                weights[:, i + j *
                        n_e_sqrt].reshape((n_in_sqrt, n_in_sqrt))

    return rearranged_weights
