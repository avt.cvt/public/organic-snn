#!/usr/bin/env python3
# %%
import os
import tempfile
import uuid

import cv2
import matplotlib.pyplot as plt
import numpy as np

tdir = tempfile.gettempdir()

plt.style.use('cvt.mplstyle')

# %%


def draw_image(wf):
    if not os.path.exists(wf):
        return None
    weights = np.load(wf)

    def weight_2d_arrangement():
        plt.figure()
        n_e_sqrt = int(np.sqrt(100))
        n_in_sqrt = int(np.sqrt(28*28))
        num_values_col = n_e_sqrt*(n_in_sqrt)
        num_values_row = num_values_col
        rearranged_weights = np.nan * np.ones((num_values_col, num_values_row))

        for i in range(n_e_sqrt):
            for j in range(n_e_sqrt):
                rearranged_weights[i*n_in_sqrt: (i+1)*n_in_sqrt, j*n_in_sqrt: (j+1)*n_in_sqrt] = \
                    weights[:, i + j *
                            n_e_sqrt].reshape((n_in_sqrt, n_in_sqrt))

        return rearranged_weights

    im = plt.imshow(weight_2d_arrangement(),
                    interpolation="nearest", vmin=0, cmap='hot_r')  # cubehelix_r
    plt.clim(0, 1)
    plt.xticks([])
    plt.yticks([])
    impath = f'{tdir}/weightplot{uuid.uuid4()}.png'
    print(impath)
    plt.savefig(impath)
    plt.close()
    return impath


# %%
def renderVid(images, name):
    frame = cv2.imread(images[0])
    height, width, layers = frame.shape

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    video = cv2.VideoWriter(
        f'plots/video{name}.mp4', fourcc, 1, (width, height))

    for image in images:
        video.write(cv2.imread(image))

    cv2.destroyAllWindows()
    video.release()


# %%
ts = [0, *np.logspace(3, 5, num=9)]
print([f'weightAge/weightsAge{t}.npy' for t in ts])
# %%
images = list(
    filter(None, [draw_image(f'weightAge/weightsAge{t}.npy') for t in ts]))

renderVid(images, 'Age')

# %%
ts = [0, *np.logspace(3, 5, num=9)]
print([f'weightAge/weightsAge{t}.npy' for t in ts])
# %%
ts = [i*1000 for i in range(101)]

images = list(
    filter(None, [draw_image(f'ageWithSTDP/weights{t}.npy') for t in ts]))

renderVid(images, 'STDPAge')

# %%
