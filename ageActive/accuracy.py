# %%
import os
import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
from config import runId
from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import set_size

plt.style.use('../cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

intensityScale = 2

# %%


def getAssignments(t):
    adpath = f'{cdir}/eval/{runId}/assigndata{intensityScale}_{t}.npy' if t > 0 else f'{cdir}/../inputs/driftingAssignments.npy'
    if not os.path.exists(adpath):
        print(adpath, 'does not exist')
        return None
    ad = np.load(adpath)
    input_numbers = y_train[:ad.shape[0]]

    assignments = np.ones(100) * -1

    maximum_rate = [0] * 100

    for j in range(10):
        rate = np.sum(ad[input_numbers == j],
                      axis=0) / (input_numbers == j).sum()
        for i in range(100):
            if rate[i] > maximum_rate[i]:
                maximum_rate[i] = rate[i]
                assignments[i] = j

    plt.scatter(range(100), assignments, color='black')
    plt.ylabel('Assignment')
    plt.xlabel('Neuron')
    plt.show()
    return assignments


# %%


def getRecognizedNumber(spike_rates, assignments):
    summed_rates = [(np.sum(spike_rates[assignments == i]) / (assignments == i).sum(
    ) if (assignments == i).sum() > 0 else -1) for i in range(10)]
    res = np.argsort(summed_rates)[-1]
    return res if summed_rates[res] > 0 else -1


# %%
ts = [0, 1000, 2000, 3000, 6000, 10000, 18000, 32000, 56000, 99000]

accuracy = []

for t in ts:
    if not os.path.exists(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy'):
        accuracy.append(0)
        continue
    ed = np.load(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy')
    assignments = getAssignments(t)
    rn = np.array([getRecognizedNumber(ed[i, :], assignments)
                   for i in range(ed.shape[0])])
    print(f'{t} {(rn == y_test[:ed.shape[0]]).mean()}')
    accuracy.append((rn == y_test[:ed.shape[0]]).mean())

    im = plt.imshow(ed)
    plt.colorbar(im)
    plt.title('evaldata' + str(t))
    plt.xlabel('Neuron')
    plt.ylabel('Image')
    plt.show()


# %%
plt.scatter(ts, accuracy, color='black')
plt.xlabel('Time')
plt.ylabel('Accuracy')
plt.yticks([0, 0.5, 1])
#plt.xticks([0, 20000, 40000])
plt.tight_layout()
set_size()
plt.show()

# %% Accuracy with original assignments

assignments = getAssignments(0)

# %%
accNoReassign = []
for t in ts:
    if not os.path.exists(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy'):
        accNoReassign.append(0)
        continue
    ed = np.load(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy')
    rn = np.array([getRecognizedNumber(ed[i, :], assignments)
                   for i in range(ed.shape[0])])
    print(f'{t} {(rn == y_test[:ed.shape[0]]).mean()}')
    accNoReassign.append((rn == y_test[:ed.shape[0]]).mean())


# %%
assignments = getAssignments(1000)

# %%
accNoReassign2 = []
for t in ts:
    if not os.path.exists(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy'):
        accNoReassign2.append(0)
        continue
    ed = np.load(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy')
    rn = np.array([getRecognizedNumber(ed[i, :], assignments)
                   for i in range(ed.shape[0])])
    print(f'{t} {(rn == y_test[:ed.shape[0]]).mean()}')
    accNoReassign2.append((rn == y_test[:ed.shape[0]]).mean())

# %%
plt.scatter(ts, accuracy, color='black', label='Reassigned')
plt.scatter(ts, accNoReassign, color='red', label='Original assignments')
plt.scatter(ts, accNoReassign2, color='blue', label='1k re assignments')
plt.legend()
# %%

acc = {
    'accuracy': accuracy,
    'ts': ts,
    'accNoReassign': accNoReassign,
    'acc1kReassign': accNoReassign2
}
pickle.dump(acc, open(f'{cdir}/accuracy.pickle', 'wb'))
