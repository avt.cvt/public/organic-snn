# %%
import os
import sys

from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

sys.path.insert(0, '..')
if True:
    from Model import *

cdir = os.path.dirname(os.path.realpath(__file__))

argv = dict(enumerate(sys.argv))
t = argv.get(1, '0.0')

model = Model()
model.net.restore(filename=f'{cdir}/../inputs/stateAfterDriftingTraining.b2')
model.setWeights(np.load(f'{cdir}/weights/weightsAge{t}.npy'))


# %%
features = []
intensityScale = 2
for i in range(200):
    print(f'Evaluating {i}')
    f = model.evaluate(X_test[i], intensity_scale=intensityScale)
    features.append(f)


features = np.array(features)
np.save(f'{cdir}/eval/evaldata{intensityScale}_{t}.npy', features)
