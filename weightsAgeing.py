# %%
import os
import sys

import matplotlib.pyplot as plt
import numpy as np

from ageActive.config import runId as runIdActive

plt.style.use('cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import weight_2d_arrangement


def plot_weights(ax, suffix='0', folder=''):
    plt.sca(ax)
    try:
        weights = np.load(f'{cdir}/{folder}/weights{suffix}.npy')
    except Exception as e:
        print(e)
        weights = np.zeros((28**2, 100))

    im = plt.imshow(weight_2d_arrangement(weights),
                    interpolation="nearest", vmin=0, cmap='hot_r')
    plt.clim(0, 1)
    plt.xticks([])
    plt.yticks([])

    print('Average', np.mean(weights))
    print('Max', np.max(weights))

    return im


# %%
fig, axes = plt.subplots(figsize=(15, 3), nrows=2, ncols=10)
im = plot_weights(axes[0, 0], f'Age{0.0}', folder='ageIdle/weights')
for i, t in enumerate(np.logspace(3, 5, num=9)):
    plot_weights(axes[0, i+1], f'Age{t}', folder='ageIdle/weights')

im = plot_weights(axes[1, 0], f'Age{0.0}', folder='ageIdle/weights')
for i, v in enumerate([1000, 2000, 3000, 6000, 10000, 18000, 32000, 56000, 99000]):
    plot_weights(axes[1, i+1], f'{int(v)}',
                 folder='ageActive/weights/' + runIdActive)

fig.subplots_adjust(right=0.995, left=0.005, top=1,
                    bottom=0, wspace=0.1, hspace=0.1)

plt.savefig(f'{cdir}/plots/ageingWeights.pdf')


# %% Presentation figure
fig, axes = plt.subplots(figsize=(6, 3), nrows=2, ncols=4)
im = plot_weights(axes[0, 0], f'Age{0.0}', folder='ageIdle/weights')
for i, t in enumerate(np.logspace(3, 5, num=3)):
    plot_weights(axes[0, i+1], f'Age{t}', folder='ageIdle/weights')

im = plot_weights(axes[1, 0], f'Age{0.0}', folder='ageIdle/weights')
for i, v in enumerate([1000, 10000, 99000]):
    plot_weights(axes[1, i+1], f'{int(v)}',
                 folder='ageActive/weights/' + runIdActive)

fig.subplots_adjust(right=0.995, left=0.005, top=1,
                    bottom=0, wspace=0.1, hspace=0.1)

plt.savefig(f'{cdir}/plots/ageingWeightsPresentation.png')

# %%
