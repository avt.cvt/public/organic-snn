# Organic SNN for MNIST

This repository provides research data for *Spiking Neural Networks Compensate Weight Drift in Organic Neuromorphic Device Networks* (http://dx.doi.org/10.1088/2634-4386/accd90).