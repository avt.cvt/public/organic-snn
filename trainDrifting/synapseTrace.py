# %%
import os
import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import set_size

plt.style.use('../cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

runId = 'run_drifting_32c946af60214abc8b4c06f1b33ef381'

# %%
sample = np.random.randint(0, 100*28*28, size=256)

# %%

ts = [t*1000 for t in range(41)]

weights = {
    t: np.load(f'weights/{runId}/trainfull{t}.npy').flatten() for t in ts
}
# %%
for s in sample:
    plt.plot(ts, [weights[t][s]
             for t in ts], color='black', alpha=.1, linewidth=.8)

plt.xlim([0, 40000])
plt.xticks([0, 20000, 40000])
plt.yticks([0,0.2,0.4,0.6,0.8, 1])
plt.xlabel('Training examples')
plt.ylabel('Weight')

plt.tight_layout()
set_size()
plt.savefig(f'{cdir}/../plots/trainDriftingSynapseTrace.pdf')
# %%
