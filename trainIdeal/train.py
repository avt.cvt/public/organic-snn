import os
import sys
from uuid import uuid4

from keras.datasets import mnist

sys.path.insert(0, '..')
if True:
    from Model import *

(X_train, y_train), (X_test, y_test) = mnist.load_data()

train_items = 40000

runid = 'run' + uuid4().hex
os.mkdir(f'weights/{runid}')
os.mkdir(f'states/{runid}')

seed(0)

model = Model()
model.net.store(filename=f'states/{runid}/trainfull{0}.b2')
np.save(f'weights/trainfull{0}.npy', model.getWeights())

for i in range(train_items):
    model.train(X_train[i])
    if ((i+1) % 1000 == 0):
        model.net.store(filename=f'states/{runid}/trainfull{i+1}.b2')
        np.save(f'weights/trainfull{i+1}.npy', model.getWeights())
    print(f'Done with {i+1} of {train_items}')
