# %%
import pickle

import matplotlib.pyplot as plt
import numpy as np

from helper.plotHelper import set_size

plt.style.use('./cvt.mplstyle')

accIdeal = pickle.load(
    open(f'trainIdeal/accuracy.pickle', 'rb'))

accDrifting = pickle.load(
    open(f'trainDrifting/accuracy.pickle', 'rb'))

# %%
plt.scatter(accIdeal['ts'], accIdeal['accuracy'], color='black', facecolors='none',
            edgecolors='black', linewidths=2)
plt.scatter(accDrifting['ts'], accDrifting['accuracy'], marker='s',
            facecolors='none', edgecolors='black', linewidths=2)
plt.scatter(accIdeal['ts'][0], accIdeal['accuracy'][0], marker='s',
            facecolors='none', edgecolors='black', linewidths=2)
plt.xlabel('Training examples')
plt.ylabel('Accuracy')
plt.yticks([0, 0.5, 1])
plt.xticks([0, 20000, 40000])
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyTrain.pdf')
plt.savefig(f'plots/accuracyTrain.png')

# %%
accIdeal['accuracy']

# %%
accDrifting['accuracy']
# %%
# %%
plt.scatter(accIdeal['ts'], accIdeal['accuracy'], color='black', marker='.')
plt.scatter(accDrifting['ts'], accDrifting['accuracy'], marker='s',
            facecolors='none', edgecolors='black', linewidths=2, alpha=0)
plt.scatter(accIdeal['ts'][0], accIdeal['accuracy'][0], marker='s',
            facecolors='none', edgecolors='black', linewidths=2, alpha=0)
plt.xlabel('Training examples')
plt.ylabel('Accuracy')
plt.yticks([0, 0.5, 1])
plt.xticks([0, 20000, 40000])
plt.ylim(-0.05, 1)
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyTrainIdeal.png')

# %%
plt.scatter(accIdeal['ts'], accIdeal['accuracy'], color='black', marker='.')
plt.scatter(accDrifting['ts'], accDrifting['accuracy'], marker='s',
            facecolors='none', edgecolors='black', linewidths=2)
plt.scatter(accIdeal['ts'][0], accIdeal['accuracy'][0], marker='s',
            facecolors='none', edgecolors='black', linewidths=2)
plt.xlabel('Training examples')
plt.ylabel('Accuracy')
plt.yticks([0, 0.5, 1])
plt.xticks([0, 20000, 40000])
plt.ylim(-0.05, 1)
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyTrainFull.png')

# %%
