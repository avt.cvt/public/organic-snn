# %%
import os
import sys

from keras.datasets import mnist
from scipy.integrate import odeint

(X_train, y_train), (X_test, y_test) = mnist.load_data()

sys.path.insert(0, '..')
if True:
    from Model import *

cdir = os.path.dirname(os.path.realpath(__file__))

runId = 'run_ageactive_1000_v2_int1'

os.makedirs(f'{cdir}/states/{runId}', exist_ok=True)
os.makedirs(f'{cdir}/weights/{runId}', exist_ok=True)

# %%


def ageWeights(weights, t):

    def age(w0):
        def dwdt(w, _):
            return -4e-7*(np.exp(8*w) - np.exp(-8*w))

        return odeint(dwdt, w0, [0, t])[1]

    for (i, j), w in np.ndenumerate(weights):
        weights[i, j] = age(w)

    return weights


# %%
model = Model()
model.net.restore(filename=f'{cdir}/../inputs/stateAfterDriftingTraining.b2')

# Todo: better integration!
# %%
idx = 0
for j in range(100):
    w = model.getWeights()
    model.setWeights(ageWeights(w, 1000))
    for i in range(1000):
        model.train(X_test[idx % X_test.shape[0]],
                    initital_input_intensity=1)
        idx += 1
        print(idx)
    model.net.store(filename=f'{cdir}/states/{runId}/state{(j+1)*1000}.b2')
    np.save(f'{cdir}/weights/{runId}/weights{(j+1)*1000}.npy',
            model.getWeights())
    print(f'Time is {(j+1)*1000}')
