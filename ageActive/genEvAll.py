# %%
import numpy as np

ts = [0, 1000, 2000, 3000, 6000, 10000, 18000, 32000, 56000, 99000]

with open('assignAll.sh', 'w') as f:
    for t in ts:
        f.write(f'python3 assign.py {t} &\n')

with open('evAll.sh', 'w') as f:
    for t in ts:
        f.write(f'python3 evaluate.py {t} &\n')
