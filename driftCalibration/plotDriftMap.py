# %%
import os
import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import set_size

cdir = os.path.dirname(os.path.realpath(__file__))

plt.style.use(f'{cdir}/../cvt.mplstyle')

map = np.load('alzheimersMap.npy')


plt.plot(map[:, 0], map[:, 1] * 1e3, color='black')
plt.yticks([-0.4, -0.2, 0])
plt.ylabel('Drift after 0.5 s ($10^{-3}$)')
plt.xticks([0, 0.5, 1])
plt.xlabel('Weight')
plt.tight_layout()
set_size()
plt.savefig(f'{cdir}/../plots/driftMap.pdf')
