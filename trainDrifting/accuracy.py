# %%
import os
import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
from keras.datasets import mnist

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import set_size

(X_train, y_train), (X_test, y_test) = mnist.load_data()


plt.style.use('../cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

runId = 'run_drifting_32c946af60214abc8b4c06f1b33ef381'

intensityScale = 2

# %%


def getAssignments(t):
    ad = np.load(f'{cdir}/eval/{runId}/assigndata{intensityScale}_{t}.npy')
    input_numbers = y_train[:ad.shape[0]]

    assignments = np.ones(100) * -1

    maximum_rate = [0] * 100

    for j in range(10):
        rate = np.sum(ad[input_numbers == j],
                      axis=0) / (input_numbers == j).sum()
        for i in range(100):
            if rate[i] > maximum_rate[i]:
                maximum_rate[i] = rate[i]
                assignments[i] = j

    plt.scatter(range(100), assignments, color='black')
    plt.ylabel('Assignment')
    plt.xlabel('Neuron')
    plt.show()
    return assignments


# %%


def getRecognizedNumber(spike_rates, assignments):
    summed_rates = [(np.sum(spike_rates[assignments == i]) / (assignments == i).sum(
    ) if (assignments == i).sum() > 0 else -1) for i in range(10)]
    res = np.argsort(summed_rates)[-1]
    return res if summed_rates[res] > 0 else -1


# %%
ts = [i*4000 for i in range(11)]

accuracy = []

for t in ts:
    edpath = f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy'
    if not os.path.exists(edpath):
        accuracy.append(np.NaN)
        continue
    ed = np.load(edpath)
    assignments = getAssignments(t)
    rn = np.array([getRecognizedNumber(ed[i, :], assignments)
                   for i in range(ed.shape[0])])
    print(f'{t} {(rn == y_test[:ed.shape[0]]).mean()}')
    accuracy.append((rn == y_test[:ed.shape[0]]).mean())

    im = plt.imshow(ed)
    plt.colorbar(im)
    plt.title('evaldata' + str(t))
    plt.xlabel('Neuron')
    plt.ylabel('Image')
    plt.show()


# %%
plt.scatter(ts, accuracy, color='black')
plt.xlabel('Time')
plt.ylabel('Accuracy')
plt.yticks([0, 0.5, 1])
plt.xticks([0, 20000, 40000])
plt.tight_layout()
set_size()
plt.show()

# %%
acc = {
    'accuracy': accuracy,
    'ts': ts
}
pickle.dump(acc, open(f'{cdir}/accuracy.pickle', 'wb'))
