#!/usr/local_rwth/bin/zsh
 
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4G
#SBATCH --job-name=spikenn
 
#SBATCH --output=output.%J.txt

#SBATCH -t 80:00:00
 
module load python
python3 trainFullDrift.py