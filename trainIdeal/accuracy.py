# %%
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
from config import runId
from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()


plt.style.use('../cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

intensityScale = 2

# %%


def getAssignments(t):
    adpath = f'{cdir}/eval/{runId}/assigndata{intensityScale}_{t}.npy'
    if not os.path.exists(adpath):
        return None
    ad = np.load(adpath)
    input_numbers = y_train[:ad.shape[0]]

    assignments = np.ones(100) * -1

    maximum_rate = [0] * 100

    for j in range(10):
        rate = np.sum(ad[input_numbers == j],
                      axis=0) / (input_numbers == j).sum()
        for i in range(100):
            if rate[i] > maximum_rate[i]:
                maximum_rate[i] = rate[i]
                assignments[i] = j

    plt.scatter(range(100), assignments, color='black')
    plt.ylabel('Assignment')
    plt.xlabel('Neuron')
    plt.show()
    return assignments


# %%
assignments = getAssignments(0)
# %%


def getRecognizedNumber(spike_rates, assignments):
    summed_rates = [(np.sum(spike_rates[assignments == i]) / (assignments == i).sum(
    ) if (assignments == i).sum() > 0 else -1) for i in range(10)]
    res = np.argsort(summed_rates)[-1]
    return res if summed_rates[res] > 0 else -1


# %%
ts = [i*4000 for i in range(11)]

accuracy = []

for t in ts:
    edpath = f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy'
    if not os.path.exists(edpath):
        accuracy.append(np.NaN)
        continue
    ed = np.load(edpath)
    assignments = getAssignments(t)
    rn = np.array([getRecognizedNumber(ed[i, :], assignments)
                   for i in range(ed.shape[0])])
    print(f'{t} {(rn == y_test[:ed.shape[0]]).mean()}')
    accuracy.append((rn == y_test[:ed.shape[0]]).mean())

    im = plt.imshow(ed)
    plt.colorbar(im)
    plt.title('evaldata' + str(t))
    plt.xlabel('Neuron')
    plt.ylabel('Image')
    plt.show()

# %%


# %%
plt.scatter(ts, accuracy, color='black', facecolors='none',
            edgecolors='black', linewidths=2)

plt.xlabel('Training examples')
plt.ylabel('Accuracy')
plt.yticks([0, 0.5, 1])
plt.xticks([0, 20000, 40000])
plt.tight_layout()

# %%

acc = {
    'accuracy': accuracy,
    'ts': ts,
}
pickle.dump(acc, open(f'{cdir}/accuracy.pickle', 'wb'))
