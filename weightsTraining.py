# %%
import matplotlib.pyplot as plt
import numpy as np
from helper.plotHelper import set_size

plt.style.use('cvt.mplstyle')


def plot_weights(ax, wf='inputs/initialWeights.npy'):
    plt.sca(ax)
    try:
        weights = np.load(wf)
    except:
        weights = np.zeros((28**2, 100))

    def weight_2d_arrangement():
        n_e_sqrt = int(np.sqrt(100))
        n_in_sqrt = int(np.sqrt(28**2))
        num_values_col = n_e_sqrt*(n_in_sqrt)
        num_values_row = num_values_col
        rearranged_weights = np.nan * np.ones((num_values_col, num_values_row))

        for i in range(n_e_sqrt):
            for j in range(n_e_sqrt):
                rearranged_weights[i*n_in_sqrt: (i+1)*n_in_sqrt, j*n_in_sqrt: (j+1)*n_in_sqrt] = \
                    weights[:, i + j *
                            n_e_sqrt].reshape((n_in_sqrt, n_in_sqrt))

        return rearranged_weights

    im = plt.imshow(weight_2d_arrangement(),
                    interpolation="nearest", vmin=0, cmap='hot_r')
    plt.clim(0, 1)
    plt.xticks([])
    plt.yticks([])

    return im


# %%
fig, axes = plt.subplots(figsize=(15, 3), nrows=2, ncols=10)
im = plot_weights(axes[0, 0])
for i in range(10):
    plot_weights(axes[0, i], f'trainIdeal/weights/trainfull{i*2000}.npy')
plot_weights(axes[1, 0])
for i in range(1, 10):
    plot_weights(
        axes[1, i], f'trainDrifting/weights/run_drifting_32c946af60214abc8b4c06f1b33ef381/trainfull{i*2000}.npy')

fig.subplots_adjust(right=0.995, left=0.005, top=1,
                    bottom=0, wspace=0.1, hspace=0.1)

plt.savefig(f'plots/weightLearning20k.pdf')

# %%
plot_weights(plt.gca(), f'trainDrifting/weights/run_drifting_32c946af60214abc8b4c06f1b33ef381/trainfull{0*2000}.npy')
plt.gca().axis('off')
plt.gcf().subplots_adjust(right=1, left=0, top=1,
                    bottom=0)
plt.savefig(f'plots/weightLearningStep0k.pdf')
# %%
plot_weights(plt.gca(), f'trainDrifting/weights/run_drifting_32c946af60214abc8b4c06f1b33ef381/trainfull{1*2000}.npy')
plt.gca().axis('off')
plt.gcf().subplots_adjust(right=1, left=0, top=1,
                    bottom=0)
plt.savefig(f'plots/weightLearningStep2k.pdf')
# %%
plot_weights(plt.gca(), f'trainDrifting/weights/run_drifting_32c946af60214abc8b4c06f1b33ef381/trainfull{10*2000}.npy')
plt.gca().axis('off')
plt.gcf().subplots_adjust(right=1, left=0, top=1,
                    bottom=0)
plt.savefig(f'plots/weightLearningStep20k.pdf')
# %%
