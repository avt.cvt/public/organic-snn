# %%
import pickle

import matplotlib.pyplot as plt
import numpy as np

from helper.plotHelper import set_size

plt.style.use('./cvt.mplstyle')

accActive = pickle.load(
    open(f'ageActive/accuracy.pickle', 'rb'))

accIdle = pickle.load(
    open(f'ageIdle/accuracy.pickle', 'rb'))

# %%
plt.axhline(y=accIdle['acc0'], color='gray', linestyle='--', zorder=0)
plt.scatter(np.array(accIdle['ts'])/60**2, accIdle['accuracy'], color='black', facecolors='none',
            edgecolors='black', linewidths=2)
plt.scatter(np.array(accActive['ts'])/60**2, accActive['accuracy'], marker='s',
            facecolors='none', edgecolors='gray', linewidths=2)
# plt.scatter(accActive['ts'], accActive['accNoReassign'], marker='^',
#             facecolors='none', edgecolors='gray', linewidths=2)
plt.scatter(np.array(accActive['ts'])/60**2, accActive['acc1kReassign'], marker='*',
            facecolors='black')
plt.xlabel('Time (h)')
plt.ylabel('Accuracy')
plt.xscale('log')
plt.yticks([0, 0.5, 1])
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyAge.pdf')

# %%

accIdle['acc0']
# %%
print('min', np.min(accActive['accuracy'][1:]))
print('max', np.max(accActive['accuracy']))


# %% Presentation Frame 0
plt.axhline(y=accIdle['acc0'], color='gray', linestyle='--', zorder=0)
plt.scatter(np.array(accIdle['ts'])/60**2, accIdle['accuracy'], color='black', facecolors='none',
            edgecolors='black', linewidths=2, alpha=0)
plt.scatter(np.array(accActive['ts'])/60**2, accActive['accuracy'], marker='s',
            facecolors='none', edgecolors='gray', linewidths=2, alpha=0)
# plt.scatter(accActive['ts'], accActive['accNoReassign'], marker='^',
#             facecolors='none', edgecolors='gray', linewidths=2)
plt.scatter(np.array(accActive['ts'])/60**2, accActive['acc1kReassign'], marker='*',
            facecolors='black', alpha=0)
plt.xlabel('Time (h)')
plt.ylabel('Accuracy')
plt.xscale('log')
plt.yticks([0, 0.5, 1])
plt.ylim(-0.05, 1)
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyAgeFrame0.png')

# %% Presentation Frame 1
plt.axhline(y=accIdle['acc0'], color='gray', linestyle='--', zorder=0)
plt.scatter(np.array(accIdle['ts'])/60**2, accIdle['accuracy'], color='black', facecolors='none',
            edgecolors='black', linewidths=2, marker='p')
plt.scatter(np.array(accActive['ts'])/60**2, accActive['accuracy'], marker='s',
            facecolors='none', edgecolors='gray', linewidths=2, alpha=0)
plt.xlabel('Time (h)')
plt.ylabel('Accuracy')
plt.xscale('log')
plt.yticks([0, 0.5, 1])
plt.ylim(-0.05, 1)
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyAgeFrame1.png')

# %% Presentation Frame 2
plt.axhline(y=accIdle['acc0'], color='gray', linestyle='--', zorder=0)
plt.scatter(np.array(accIdle['ts'])/60**2,
            accIdle['accuracy'], color='black', marker="p", facecolors='none', linewidths=2, edgecolors="black")
plt.scatter(np.array(accActive['ts'])/60**2, accActive['accuracy'], marker='s',
            facecolors='none', linewidths=2, edgecolors="black")
plt.xlabel('Time (h)')
plt.ylabel('Accuracy')
plt.xscale('log')
plt.yticks([0, 0.5, 1])
plt.ylim(-0.05, 1)
plt.tight_layout()
set_size()
plt.savefig(f'plots/accuracyAgeFrame2.png')
# %%
