# %%
import numpy as np

ts = np.insert(np.logspace(3, 5, num=9), 0, 0)

with open('evAll.sh', 'w') as f:
    for t in ts:
        f.write(f'python3 evaluate.py {t} &\n')

# %%
