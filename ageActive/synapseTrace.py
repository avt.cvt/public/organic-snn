# %%
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
from config import runId

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import set_size

plt.style.use('../cvt.mplstyle')

cdir = os.path.dirname(os.path.realpath(__file__))

# %%
sample = np.random.randint(0, 100*28*28, size=256)

ts = [i*1000 for i in range(101)]

weights = {
    t: np.load(f'weights/{runId}/weights{t}.npy').flatten() for t in ts
}
# %%
for s in sample:
    plt.plot(np.array(ts) / (60*60), [weights[t][s]
             for t in ts], color='black', alpha=.1, linewidth=.8)

plt.xticks([0, 12, 24])
plt.yticks([0,0.2,0.4,0.6,0.8, 1])
plt.xlim([0, 26])
plt.xlabel('Time (h)')
plt.ylabel('Weight')

plt.tight_layout()
set_size()
plt.savefig(f'{cdir}/../plots/ageActiveSynapseTrace.pdf')