import os

import brian2.numpy_ as np
from brian2 import *
from scipy.interpolate import interp1d
from scipy.sparse import csr_matrix

cdir = os.path.dirname(os.path.realpath(__file__))


n_input = 28*28  # input layer
n_e = 100  # e - excitatory
n_i = n_e  # i - inhibitory

v_rest_e = -65.*mV  # v - membrane potential
v_reset_e = -65.*mV
v_thresh_e = -52.*mV

v_rest_i = -60.*mV
v_reset_i = -45.*mV
v_thresh_i = -40.*mV

refrac_e = 5. * ms
refrac_i = 2. * ms

tc_pre = 20*ms
tc_post = 20*ms
eta_post = 0.01       # learning rate
exp_post = 0.2

xtar = 0.25

# presynaptic and postsynaptic traces
stdp = '''
    w : 1
    lr : 1 (shared)
    dpre/dt   =   -pre/(tc_pre)         : 1 (event-driven)
    dpost/dt  = -post/(tc_post)     : 1 (event-driven)
    '''
pre = '''
    pre = 1.
    ge_post += w
    '''
post = '''
    w = clip(w + lr*eta_post * (pre - xtar) * (1-w)**exp_post, 0, 1)
    post = 1.
    '''

tc_theta = 1e7 * ms
theta_plus_e = 0.05 * mV
reset_e = 'v = v_reset_e; theta += lre*theta_plus_e; timer = 0*ms'

offset = 20.0*mV
thresh_e = '(v>(theta - offset + -52.*mV)) and (timer>refrac_e)'


class Model():

    def __init__(self):

        driftMap = np.load(f'{cdir}/driftCalibration/alzheimersMap.npy')
        self.drift = interp1d(driftMap[:, 0], driftMap[:, 1],
                              kind='linear', fill_value="extrapolate")

        self.driftEnabled = False

        app = {}

        # input images as rate encoded Poisson generators
        app['PG'] = PoissonGroup(
            n_input, rates=np.zeros(n_input)*Hz, name='PG')

        # excitatory group
        neuron_e = '''
            lre : 1 (shared)
            dv/dt = (ge*-v + gi*(-100*mV-v) + (v_rest_e-v)) / (100*ms) : volt
            dge/dt = -ge / (1.*ms) : 1
            dgi/dt = -gi / (2.*ms) : 1
            dtimer/dt = 0.1 : second
            dtheta/dt = lre * -theta / (tc_theta)  : volt
            '''
        app['EG'] = NeuronGroup(n_e, neuron_e, threshold=thresh_e,
                                refractory=refrac_e, reset=reset_e, method='euler', name='EG')
        app['EG'].v = v_rest_e - 40.*mV
        app['EG'].theta = '20.*mV'
        app['EG'].lre = 1

        # ibhibitory group
        neuron_i = '''
            dv/dt = (ge*-v + gi*(-85.*mV-v) + (v_rest_i-v)) / (10*ms) : volt
            dge/dt = -ge / (1.*ms) : 1
            dgi/dt = -gi/(2.0*ms)  : 1
            '''
        app['IG'] = NeuronGroup(n_i, neuron_i, threshold='v>v_thresh_i',
                                refractory=refrac_i, reset='v=v_reset_i', method='euler', name='IG')
        app['IG'].v = v_rest_i - 40.*mV

        # poisson generators one-to-all excitatory neurons with plastic connections
        app['S1'] = Synapses(app['PG'], app['EG'], stdp,
                             on_pre=pre, on_post=post, method='euler', name='S1')
        app['S1'].connect()
        app['S1'].w = '(rand()+0.01)*0.3'  # random weights initialisation
        app['S1'].lr = 1  # enable stdp
        app['S1'].delay = 'rand()*10*ms'  # random delays

        # excitatory neurons one-to-one inhibitory neurons
        app['S2'] = Synapses(app['EG'], app['IG'], 'w : 1',
                             on_pre='ge_post += w', name='S2')
        app['S2'].connect(j='i')

        app['S2'].w = 10.4

        # inhibitory neurons one-to-all-except-one excitatory neurons
        app['S3'] = Synapses(app['IG'], app['EG'], 'w : 1',
                             on_pre='gi_post += w', name='S3')
        app['S3'].connect(condition='i!=j')

        app['S3'].w = 17.0

        self.net = Network(app.values())
        self.net.run(0*second)

    def __getitem__(self, key):
        return self.net[key]

    def train(self, X, initital_input_intensity=1):
        self.net['S1'].lr = 1  # stdp on
        self.net['EG'].lre = 1

        if self.driftEnabled:
            self.weightDrift()

        mon = SpikeMonitor(self.net['EG'], name='RM')
        self.net.add(mon)
        input_intensity = initital_input_intensity
        while(np.sum(mon.count) < 5):
            # active mode
            self.net['PG'].rates = X.ravel()*Hz * input_intensity / 4
            self.net.run(0.35*second)

            # passive mode
            self.net['PG'].rates = np.zeros(n_input)*Hz
            self.net.run(0.15*second)
            input_intensity += 1

        features = np.array(mon.count)

        print(f'Final input intensity: {input_intensity-1}')

        self.net.remove(self.net['RM'])

        return features

    def getWeights(self):
        return csr_matrix(
            (self['S1'].w, (self['S1'].i, self['S1'].j)), shape=(n_input, n_e)).toarray()

    def setWeights(self, w):
        self.net['S1'].w = w[self['S1'].i, self['S1'].j]

    def weightDrift(self):
        self.net['S1'].w += self.drift(self.net['S1'].w)

    def evaluate(self, X, intensity_scale=2):
        self.net['S1'].lr = 0  # stdp off
        self.net['EG'].lre = 0  # stdp off

        # rate monitor to count spikes
        mon = SpikeMonitor(self.net['EG'], name='RM')
        self.net.add(mon)

        # active mode
        self.net['PG'].rates = X.ravel()*Hz / 4 * intensity_scale
        self.net.run(0.35*second)

        # passive mode
        self.net['PG'].rates = np.zeros(n_input)*Hz
        self.net.run(0.15*second)

        # spikes per neuron for each image
        features = np.array(mon.count, dtype=int8)

        self.net.remove(self.net['RM'])

        return features
