#!/usr/bin/env python3
# %%
import os
import tempfile

import cv2
import matplotlib.pyplot as plt
import numpy as np

from helper.plotHelper import set_size, weight_2d_arrangement

tdir = tempfile.gettempdir()

plt.style.use('cvt.mplstyle')

# %%


def draw_image(suffix):
    wf = f'trainDrifting/weights/run_drifting_32c946af60214abc8b4c06f1b33ef381/trainfull{suffix}.npy'
    if not os.path.exists(wf):
        return None
    weights = np.load(wf)

    plt.figure()

    im = plt.imshow(weight_2d_arrangement(weights),
                    interpolation="nearest", vmin=0, cmap='hot_r')  # cubehelix_r
    plt.clim(0, 1)
    plt.xticks([])
    plt.yticks([])
    plt.tight_layout()
    set_size()
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                        hspace=0, wspace=0)
    impath = f'{tdir}/weightplot{suffix}.png'
    print(impath)
    plt.savefig(impath)
    plt.close()
    return impath


t = [i*1000 for i in range(0, 41)]


images = list(filter(None, [draw_image(str(i)) for i in t]))


# %%
frame = cv2.imread(images[0])
height, width, layers = frame.shape

fourcc = cv2.VideoWriter_fourcc(*'MP4V')
video = cv2.VideoWriter('plots/video.mp4', fourcc, 1, (width, height))

for image in images:
    video.write(cv2.imread(image))

cv2.destroyAllWindows()
video.release()
