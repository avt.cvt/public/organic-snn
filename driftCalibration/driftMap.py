# %%
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

# %%


def dwdt(w, _):
    return -2e-7*(np.exp(8.3*w) - np.exp(-8.3*w))


t = np.linspace(0, 1e6, num=int(1e6/0.5), endpoint=False)
w = odeint(dwdt, 1, t)[:, 0]

# %%

plt.plot(t, w)

# %% Map contains current weight and delta for 0.5 s
map = np.array([[w[i],  w[i+1] - w[i]] for i in range(len(w)-1)])

# %%
plt.plot(map[:, 0], map[:, 1])

# %%
np.save('alzheimersMap.npy', map)

# %%
