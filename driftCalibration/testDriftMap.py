# %%
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d

# %%
map = np.load('alzheimersMap.npy')

# %%
w = np.random.rand(28*28*400)

# %%

f = interp1d(map[:, 0], map[:, 1], kind='linear', fill_value="extrapolate")
# %%
delta = f(w)

# %%
plt.scatter(w, delta)
