# %%
import numpy as np

ts = [i*4000 for i in range(11)]

with open('evAll.sh', 'w') as f:
    for t in ts:
        f.write(f'python3 evaluate.py {t} &\n')

with open('assignAll.sh', 'w') as f:
    for t in ts:
        f.write(f'python3 assign.py {t} &\n')
