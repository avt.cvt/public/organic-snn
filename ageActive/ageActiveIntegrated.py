# %%
import os
import sys

from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

sys.path.insert(0, '..')
if True:
    from Model import *

cdir = os.path.dirname(os.path.realpath(__file__))

# %%
argv = dict(enumerate(sys.argv))
evperiod = int(argv.get(1, 1))  # seconds per evaluation

# 1=>50%, 2=>75%, 5=>90%, 10=>95%, 20=>97.5%, 25=>98%, 50=>99%, 100=>99.5%
idlerate = (1-0.5/evperiod)*100

runId = f'run_ageactiveintegrated_{idlerate}idle_int1'

os.makedirs(f'{cdir}/states/{runId}', exist_ok=True)
os.makedirs(f'{cdir}/weights/{runId}', exist_ok=True)


# %%
model = Model()
model.net.restore(filename=f'{cdir}/../inputs/stateAfterDriftingTraining.b2')
model.driftEnabled = True

# %%
idx = 0
for j in range(100):
    for i in range(int(1000/evperiod)):
        model.train(X_test[idx % X_test.shape[0]],
                    initital_input_intensity=1)
        for _ in range(2*evperiod - 1):
            model.weightDrift()
        idx += 1
        print(idx)
    model.net.store(filename=f'{cdir}/states/{runId}/state{(j+1)*1000}.b2')
    np.save(f'{cdir}/weights/{runId}/weights{(j+1)*1000}.npy',
            model.getWeights())
    print(f'Time is {(j+1)*1000}')
