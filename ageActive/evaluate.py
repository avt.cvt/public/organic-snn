# %%
import os
import sys

from config import runId
from keras.datasets import mnist

sys.path.insert(0, '..')
if True:
    from Model import *

os.makedirs(f'./eval/{runId}', exist_ok=True)

(X_train, y_train), (X_test, y_test) = mnist.load_data()


cdir = os.path.dirname(os.path.realpath(__file__))

argv = dict(enumerate(sys.argv))
t = argv.get(1, '0')

model = Model()
model.net.restore(filename=f'{cdir}/states/{runId}/state{t}.b2')


# %%
features = []
intensityScale = 2
for i in range(200):
    print(f'Evaluating {i}')
    f = model.evaluate(X_test[i], intensity_scale=intensityScale)
    features.append(f)


features = np.array(features)
np.save(f'{cdir}/eval/{runId}/evaldata{intensityScale}_{t}.npy', features)
