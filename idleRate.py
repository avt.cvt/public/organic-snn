# %%
import pickle

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pylab as pl
import seaborn as sns

from helper.plotHelper import set_size

plt.style.use('./cvt.mplstyle')

# %%
acc0 = pickle.load(
    open(f'inputs/idleRateDependence/100.pickle', 'rb'))['acc0']
print('Acc0', acc0)


def loadData(idleRate):
    data = pickle.load(
        open(f'inputs/idleRateDependence/{idleRate}.pickle', 'rb'))
    if data['ts'][0] == 0:
        data['accuracy'][0] = acc0
    else:
        data['accuracy'] = np.insert(np.array(data['accuracy']), 0, acc0)
        data['ts'] = np.insert(data['ts'], 0, 0)
    return data


ir50 = loadData(50)
ir90 = loadData(90)
ir95 = loadData(95)
ir98 = loadData(98)
ir99 = loadData(99)
ir100 = loadData(100)
# %%

df = pd.DataFrame(
    {'ts': ir50['ts'], 'acc': ir50['accuracy'], 'ir': "50%"})

df = df.append(pd.DataFrame(
    {'ts': ir90['ts'], 'acc': ir90['accuracy'], 'ir': "90%"}))

df = df.append(pd.DataFrame(
    {'ts': ir99['ts'], 'acc': ir99['accuracy'], 'ir': "99%"}))

df = df.append(pd.DataFrame(
    {'ts': ir100['ts'], 'acc': ir100['accuracy'], 'ir': "100%"}))

df

# %%
#df = df[df['acc'] > 0]

# %%
sns.color_palette("mako", as_cmap=True)
sns.swarmplot(data=df, x="ir", y="acc", size=10, hue="ts", palette="mako")
plt.xlabel('Idle rate')
plt.ylabel('Accuracy')
plt.ylim(0, 1)
plt.yticks([0, 0.5, 1])
plt.gca().legend_.remove()
plt.tight_layout()
set_size()
plt.savefig(f'plots/idleRate.pdf')

# %%

plt.figure(figsize=(0.2, 2))
a = np.array([[0, 1]])
img = pl.imshow(a, cmap="mako")
pl.gca().set_visible(False)
cax = pl.axes([0, 0, 1, 1])
cbar = pl.colorbar(cax=cax)
cbar.set_ticks([])
cbar.outline.set_visible(False)
pl.savefig("plots/colorbarIdleRate.pdf")

# %%
print(ir99['accuracy'])


# %%
dfe = pd.concat([pd.DataFrame({
    'max': np.max(df['accuracy']),
    'min': np.min(df['accuracy']),
    'f': ir
}, index=[0]) for df, ir in [(ir50, 50), (ir90, 90), (ir95, 95), (ir98, 98), (ir99, 99), (ir100, 100)]])
dfe

# %%

plt.plot(dfe['f'], dfe['min'], color='black',
         linewidth=2, marker='^')
plt.plot(dfe['f'], dfe['max'], color='black',
         linewidth=2, marker='v')
plt.gca().fill_between(dfe['f'], dfe['min'],
                       dfe['max'], alpha=0.1, color='black')
plt.xlim(88, 100)
plt.ylim(0, 1)
plt.yticks([0, 0.5, 1])
plt.xticks([90, 95, 100])
plt.xlabel('Idle rate')
plt.ylabel('Accuracy')
plt.gca().xaxis.set_major_formatter(mpl.ticker.PercentFormatter(decimals=0))

plt.tight_layout()
set_size()
plt.savefig(f'plots/idleRateMinMax.pdf')

# %%
