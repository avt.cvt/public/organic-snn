# %%
import os
import sys

from keras.datasets import mnist

sys.path.insert(0, '..')
if True:
    from Model import *

(X_train, y_train), (X_test, y_test) = mnist.load_data()


cdir = os.path.dirname(os.path.realpath(__file__))

runId = 'run_drifting_32c946af60214abc8b4c06f1b33ef381'

os.makedirs(f'{cdir}/eval/{runId}', exist_ok=True)

argv = dict(enumerate(sys.argv))
t = argv.get(1, '0')

# %%
if not os.path.exists(f'{cdir}/states/{runId}/trainfull{t}.b2'):
    print('No model found')
    exit()

model = Model()
model.net.restore(filename=f'{cdir}/states/{runId}/trainfull{t}.b2')

# %%
intensityScale = 2
features = []
for i in range(1000):
    print(f'Evaluating {i}')
    f = model.evaluate(X_train[i], intensity_scale=intensityScale)
    features.append(f)


features = np.array(features)
np.save(f'{cdir}/eval/{runId}/assigndata{intensityScale}_{t}.npy', features)
