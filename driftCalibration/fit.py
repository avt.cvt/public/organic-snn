#!/usr/bin/env python3
# %%

import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.integrate import odeint
from scipy.optimize import fmin

sys.path.insert(0, '..')
if True:
    from helper.plotHelper import set_size

cdir = os.path.dirname(os.path.realpath(__file__))

plt.style.use(f'{cdir}/../cvt.mplstyle')

# %%
calib = pd.read_csv(f'{cdir}/calibrationSDd.csv')

xp = np.linspace(0, 60*60*10, num=100)


def integrate(x, w0=1, t=xp):
    ef = x[0]
    pf = x[1]

    def dwdt(w, _):
        return -pf*(np.exp(ef*w) - np.exp(-ef*w))

    return odeint(dwdt, w0, t)


x0 = [8.3, 2e-7]

for k, (name, group) in enumerate(reversed(list(calib.groupby('InitialWeight')))):
    plt.scatter(group.Time, group.CurrentWeight,
                edgecolors='gray', facecolors='none')
    plt.plot(group.Time, integrate(x0,
             group.CurrentWeight.max(), group.Time), color='black')

plt.xlabel('Time (s)')
plt.ylabel('Weight')
plt.yticks([0,0.5, 1])
plt.xticks([0,600, 1200])
plt.tight_layout()
set_size()
plt.savefig(f'{cdir}/../plots/calibration.pdf')
# %%


def objective(x):
    err = 0
    for _, group in calib.groupby('InitialWeight'):
        err += (group.CurrentWeight - integrate(x,
                group.CurrentWeight.max(), group.Time)[0]).sum()
    return err


xopt = fmin(objective, x0)
print(xopt)

# %%
for sw in [1, 0.8, 0.6, 0.4, 0.2, 0]:
    plt.plot(xp / 60/60, integrate(x0, sw, xp), color='black')

plt.xlabel('Time (h)')
plt.ylabel('Weight')
plt.yticks([0,0.5, 1])
plt.xticks([0, 5, 10])
plt.tight_layout()
set_size()
plt.savefig(f'{cdir}/../plots/weightDrift.pdf')
# %%
